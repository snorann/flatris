FROM node:12-alpine

RUN mkdir /app
COPY package.json /app
COPY . /app

WORKDIR /app

RUN yarn install
#RUN yarn test
RUN yarn build
EXPOSE 3000

#ENTRYPOINT yarn start
CMD [ "yarn","start" ]
 # команда для запуска




